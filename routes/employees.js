import express from 'express';
import { 
    getAllEmp, 
    createEmployee, 
    getEmpWithID, 
    deleteEmployee,
    updateEmployee 
} from '../controllers/employees.js';

const router = express.Router();

router.get('/', getAllEmp);

router.post('/', createEmployee);

router.get('/:id', getEmpWithID);

router.delete('/:id', deleteEmployee);

router.patch('/:id', updateEmployee)

export default router;