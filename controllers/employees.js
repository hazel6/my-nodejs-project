// Get all employees and display
import { v4 as uuidv4 } from 'uuid';

let employees = [];

export const getAllEmp = (req, res) => {
    res.send(employees);
}

// Create new employee and add to databasse
export const createEmployee = (req, res) => {
    const emp = req.body;
    emp.emp_id = uuidv4();
    employees.push(emp)
    res.send(`Employee name ${emp.fname} ${emp.lname} added to the database.`);
}

// Get employee info with specific ID and display
export const getEmpWithID = (req, res) => {
    const { id } = req.params;
    const foundEmp = employees.find((emp) => emp.emp_id === id);
    res.send(foundEmp);
}

// Delete employee info with specific ID
export const deleteEmployee = (req, res) => {
    const { id } = req.params;

    // filter works if result is false
    employees = employees.filter((emp) => emp.emp_id !== id);
    res.send(`Employee with id ${id} deleted from the database.`);
}

// Update employee info with specific ID
export const updateEmployee = (req, res) => {
    const { id } = req.params;

    const { fname, lname, bdate } = req.body;

    const empForUpdate = employees.find((emp) => emp.emp_id === id);

    if (fname) empForUpdate.fname = fname;
    if (lname) empForUpdate.lname = lname;
    if (bdate) empForUpdate.bdate = bdate;

    res.send(`Employee with id ${id} hass been updated.`);

}